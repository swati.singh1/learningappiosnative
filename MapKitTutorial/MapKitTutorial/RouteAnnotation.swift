//
//  RouteAnnotation.swift
//  MapKitTutorial
//
//  Created by Swati Singh on 09/09/20.
//  Copyright © 2020 Thorn Technologies. All rights reserved.
//

import MapKit

class RouteAnnotation: NSObject {
  private let item: MKMapItem

  init(item: MKMapItem) {
    self.item = item

    super.init()
  }
}

// MARK: - MKAnnotation

extension RouteAnnotation: MKAnnotation {
  var coordinate: CLLocationCoordinate2D {
    return item.placemark.coordinate
  }

  var title: String? {
    return item.name
  }
}

