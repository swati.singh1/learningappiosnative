//
//  HomeViewController.swift
//  MapKitTutorial
//
//  Created by Swati Singh on 16/09/20.
//  Copyright © 2020 Thorn Technologies. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func locationNavigationBtn(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let directionViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(directionViewController, animated: true)
        
    }
    
    @IBAction func authenticationBtn(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let directionViewController = storyBoard.instantiateViewController(withIdentifier: "AuthenticationViewController") as! AuthenticationViewController
        self.navigationController?.pushViewController(directionViewController, animated: true)
    }

}
