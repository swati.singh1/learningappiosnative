//
//  DirectionViewController.swift
//  MapKitTutorial
//
//  Created by Swati Singh on 03/09/20.
//  Copyright © 2020 Thorn Technologies. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

@available(iOS 9.3, *)
class DirectionViewController: UIViewController, UITextFieldDelegate {
    
    var matchingItems: [MKMapItem] = []
    var mapView: MKMapView?
     private var currentPlace: CLPlacemark?
    private var currentRegion: MKCoordinateRegion?

    @IBOutlet var txtStart: UITextField!
    @IBOutlet var txtEnd: UITextField!
    
    @IBOutlet var txtDestination: UITextField!
    @IBOutlet var tableView: UITableView!
    var txtStartStreet : UITextField!
    var txtEndStreet : UITextField!
    var txtDestinationStreet : UITextField!
    
    private let locationManager = CLLocationManager()
    private let completer = MKLocalSearchCompleter()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        txtStart.delegate = self
        txtEnd.delegate = self
        txtDestination.delegate = self
        completer.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    func parseAddress(selectedItem:MKPlacemark) -> String {
        
        // put a space between "4" and "Melrose Place"
        let firstSpace = (selectedItem.subThoroughfare != nil &&
                            selectedItem.thoroughfare != nil) ? " " : ""
        
        // put a comma between street and city/state
        let comma = (selectedItem.subThoroughfare != nil || selectedItem.thoroughfare != nil) &&
                    (selectedItem.subAdministrativeArea != nil || selectedItem.administrativeArea != nil) ? ", " : ""
        
        // put a space between "Washington" and "DC"
        let secondSpace = (selectedItem.subAdministrativeArea != nil &&
                            selectedItem.administrativeArea != nil) ? " " : ""
        
        let addressLine = String(
            format:"%@%@%@%@%@%@%@",
            // street number
            selectedItem.subThoroughfare ?? "",
            firstSpace,
            // street name
            selectedItem.thoroughfare ?? "",
            comma,
            // city
            selectedItem.locality ?? "",
            secondSpace,
            // state
            selectedItem.administrativeArea ?? ""
        )
        
        return addressLine
    }
    
    private func presentAlert(message: String) {
      let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
      alertController.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))

      present(alertController, animated: true)
    }
    

    @IBAction func directionBtn(_ sender: Any) {
//        view.endEditing(true)
//
//           calculateButton.isEnabled = false
//           activityIndicatorView.startAnimating()

           let segment: RouteBuilder.Segment?
//           if let currentLocation = currentPlace?.location {
//             segment = .location(currentLocation)
//            print("Segment current \(segment)")
//           } else if
            if let originValue = txtStartStreet.contents {
             segment = .text(originValue)
            print("Segment \(segment)")
           } else {
             segment = nil
            print("Segment nil \(segment)")
           }

           let stopSegments: [RouteBuilder.Segment] = [
             txtEndStreet.contents,
             txtDestinationStreet.contents
           ]
           .compactMap { contents in
             if let value = contents {
                print("Stops \(value))")
               return .text(value)
                
             } else {
               return nil
             }
           }

           guard
             let originSegment = segment,
             !stopSegments.isEmpty
             else {
               presentAlert(message: "Please select an origin and at least 1 stop.")
               //activityIndicatorView.stopAnimating()
               //calculateButton.isEnabled = true
               return
           }
         

           RouteBuilder.buildRoute(
             origin: originSegment,
             stops: stopSegments,
             within: currentRegion
           ) { result in
             //self.calculateButton.isEnabled = true
             //self.activityIndicatorView.stopAnimating()

             switch result {
             case .success(let route):
                print("Route \(route)")
                
               let viewController = DetailViewController(route: route)
               self.present(viewController, animated: true)

             case .failure(let error):
               let errorMessage: String

               switch error {
               case .invalidSegment(let reason):
                 errorMessage = "There was an error with: \(reason)."
               }

               self.presentAlert(message: errorMessage)
             }
           }
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
//          let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//          let locationViewController = storyBoard.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
//                  self.present(locationViewController, animated: true, completion: nil)
        tableView.reloadData()
        tableView.isHidden = false
//        let searchBarText = txtStart.text
//
//        let request = MKLocalSearch.Request()
//        request.naturalLanguageQuery = searchBarText
//        //request.region = mapView.region
//        let search = MKLocalSearch(request: request)
//
//        search.start { response, _ in
//            guard let response = response else {
//                return
//            }
//            self.matchingItems = response.mapItems
//            self.tableView.reloadData()
//        }
       }
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        let searchBarText = txtStart.text
//
//        let request = MKLocalSearch.Request()
//        request.naturalLanguageQuery = searchBarText
//        //request.region = mapView.region
//        let search = MKLocalSearch(request: request)
//
//        search.start { response, _ in
//            guard let response = response else {
//                return
//            }
//            self.matchingItems = response.mapItems
//            self.tableView.reloadData()
//        }
//        return true
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var searchBarText = ""
        if txtStart.isFirstResponder{
            searchBarText = txtStart.text ?? ""
        }else if txtEnd.isFirstResponder{
            searchBarText = txtEnd.text ?? ""
        }else{
            searchBarText = txtDestination.text ?? ""
        }
        

        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchBarText
        //request.region = mapView.region
        let search = MKLocalSearch(request: request)

        search.start { response, _ in
            guard let response = response else {
                return
            }
            self.matchingItems = response.mapItems
            self.tableView.reloadData()
        }
        return true
    }
    
}
@available(iOS 9.3, *)
extension DirectionViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              let cell = tableView.dequeueReusableCell(withIdentifier: "DirectionTableViewCell")!
              let selectedItem = matchingItems[indexPath.row].placemark
              cell.textLabel?.text = selectedItem.name
              cell.detailTextLabel?.text = parseAddress(selectedItem: selectedItem)
              return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedItem = matchingItems[indexPath.row].placemark
        print("Places \(selectedItem)")
        if txtStart.isFirstResponder {
            //txtStartStreet.text = selectedItem.thoroughfare
            txtStart.text = selectedItem.name
        }else if txtEnd.isFirstResponder{
            //txtEndStreet.text = selectedItem.subLocality
            txtEnd.text = selectedItem.name
        }else {
            //txtDestinationStreet.text = selectedItem.subLocality
            txtDestination.text = selectedItem.name
        }
        //print("\(selectedItem.name)")
        tableView.isHidden = true
                 // handleMapSearchDelegate?.dropPinZoomIn(placemark: selectedItem)
                  //dismiss(animated: true, completion: nil)
       }
    
    
}

// MARK: - CLLocationManagerDelegate

@available(iOS 9.3, *)
extension DirectionViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    guard status == .authorizedWhenInUse else {
      return
    }

    manager.requestLocation()
  }

  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let firstLocation = locations.first else {
      return
    }

    let commonDelta: CLLocationDegrees = 25 / 111 // 1/111 = 1 latitude km
    let span = MKCoordinateSpan(latitudeDelta: commonDelta, longitudeDelta: commonDelta)
    let region = MKCoordinateRegion(center: firstLocation.coordinate, span: span)

    currentRegion = region
    completer.region = region

    CLGeocoder().reverseGeocodeLocation(firstLocation) { places, _ in
      guard let firstPlace = places?.first, self.txtStartStreet.contents == nil else {
        return
      }

      self.currentPlace = firstPlace
      self.txtStartStreet.text = firstPlace.abbreviation
    }
  }

  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print("Error requesting location: \(error.localizedDescription)")
  }
}

// MARK: - MKLocalSearchCompleterDelegate

@available(iOS 9.3, *)
extension DirectionViewController: MKLocalSearchCompleterDelegate {
    @available(iOS 9.3, *)
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
    guard let firstResult = completer.results.first else {
      return
    }

    //showSuggestion(firstResult.title)
  }

    @available(iOS 9.3, *)
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
    print("Error suggesting a location: \(error.localizedDescription)")
  }
}
