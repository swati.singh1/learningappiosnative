//
//  UITextField+Addition.swift
//  MapKitTutorial
//
//  Created by Swati Singh on 09/09/20.
//  Copyright © 2020 Thorn Technologies. All rights reserved.
//


import UIKit

extension UITextField {
  var contents: String? {
    guard
      let text = text?.trimmingCharacters(in: .whitespaces),
      !text.isEmpty
      else {
        return nil
    }

    return text
  }
}
