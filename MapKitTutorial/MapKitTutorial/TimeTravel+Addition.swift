//
//  TimeTravel+Addition.swift
//  MapKitTutorial
//
//  Created by Swati Singh on 09/09/20.
//  Copyright © 2020 Thorn Technologies. All rights reserved.
//

import Foundation
extension TimeInterval {
  var formatted: String {
    let formatter = DateComponentsFormatter()
    formatter.unitsStyle = .full
    formatter.allowedUnits = [.hour, .minute]

    return formatter.string(from: self) ?? ""
  }
}
