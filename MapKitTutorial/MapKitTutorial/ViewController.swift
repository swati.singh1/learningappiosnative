//
//  ViewController.swift
//  MapKitTutorial
//
//  Created by Robert Chen on 12/23/15.
//  Copyright © 2015 Thorn Technologies. All rights reserved.
//

import UIKit
import MapKit

protocol HandleMapSearch: class {
    func dropPinZoomIn(placemark:MKPlacemark)
}

class ViewController: UIViewController {
    
    var selectedPin: MKPlacemark?
    var resultSearchController: UISearchController!
    var lat = 0.0
    var long = 0.0
    var destinationLat = 0.0
    var destinationLong = 0.0
    var destinationTitle = ""
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var mapView: MKMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController.searchResultsUpdater = locationSearchTable
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Search for places"
        navigationItem.titleView = resultSearchController?.searchBar
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        locationSearchTable.mapView = mapView
        locationSearchTable.handleMapSearchDelegate = self

        
    }
    
    @objc func getDirections(){
        guard let selectedPin = selectedPin else { return }
        let mapItem = MKMapItem(placemark: selectedPin)
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        mapItem.openInMaps(launchOptions: launchOptions)
    }
    
    @available(iOS 9.3, *)
    @IBAction func directionBtn(_ sender: Any) {
//                let sourceLocation = CLLocationCoordinate2D(latitude: lat, longitude: long)
//                let destinationLocation = CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLong)
//
//                // 3.
//                let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
//                let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
//
//                // 4.
//                let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
//                let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
//
//                // 5.
//                let sourceAnnotation = MKPointAnnotation()
//                sourceAnnotation.title = "Your Location"
//
//                if let location = sourcePlacemark.location {
//                    sourceAnnotation.coordinate = location.coordinate
//                }
//
//
//                let destinationAnnotation = MKPointAnnotation()
//                destinationAnnotation.title = destinationTitle
//
//                if let location = destinationPlacemark.location {
//                    destinationAnnotation.coordinate = location.coordinate
//                }
//
//                // 6.
//                self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
//
//                // 7.
//                let directionRequest = MKDirectionsRequest()
//                directionRequest.source = sourceMapItem
//                directionRequest.destination = destinationMapItem
//                directionRequest.transportType = .automobile
//
//                // Calculate the direction
//                let directions = MKDirections(request: directionRequest)
//
//                // 8.
//                directions.calculate {
//                    (response, error) -> Void in
//
//                    guard let response = response else {
//                        if let error = error {
//                            print("Error: \(error)")
//                        }
//
//                        return
//                    }
//
//                    let route = response.routes[0]
//                    print("response.routes[0]\(response.routes)")
//                    self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
//
//                    let rect = route.polyline.boundingMapRect
//                    self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
//                }
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let directionViewController = storyBoard.instantiateViewController(withIdentifier: "DirectionViewController") as! DirectionViewController
        self.navigationController?.pushViewController(directionViewController, animated: true)
                //self.present(locationViewController, animated: true, completion: nil)

    }
}
extension ViewController : CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
         print("error:: \(error.localizedDescription)")
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

//        if locations.first != nil {
//            print("location:: (location)")
//        }
        guard let location = locations.first else { return }
        lat = location.coordinate.latitude
        long = location.coordinate.longitude
        print("current.latitude\(location.coordinate.latitude)")
        print("current.long\(location.coordinate.longitude)")
        
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
               let region = MKCoordinateRegion(center: location.coordinate, span: span)
               mapView.setRegion(region, animated: true)

    }

}

extension ViewController: HandleMapSearch {
    
    func dropPinZoomIn(placemark: MKPlacemark){
        // cache the pin
        selectedPin = placemark
        // clear existing pins
        mapView.removeAnnotations(mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        destinationLat = annotation.coordinate.latitude
        destinationLong = annotation.coordinate.longitude
        print("annotation.coordinate.latitude\(annotation.coordinate.latitude)")
        print("annotation.coordinate.long\(annotation.coordinate.longitude)")
        annotation.title = placemark.name
        destinationTitle = annotation.title ?? ""
        
        if let city = placemark.locality,
            let state = placemark.administrativeArea {
                annotation.subtitle = "\(city) \(state)"
        }
        
        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: placemark.coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
    
}

extension ViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        
        guard !(annotation is MKUserLocation) else { return nil }
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        }
        pinView?.pinTintColor = UIColor.orange
        pinView?.canShowCallout = true
        let smallSquare = CGSize(width: 30, height: 30)
        
        let button = UIButton(frame: CGRect(origin: CGPoint(x:0,y:0), size: smallSquare))
        button.setBackgroundImage(UIImage(named: "car"), for: .normal)
        button.addTarget(self, action: #selector(ViewController.getDirections), for: .touchUpInside)
        pinView?.leftCalloutAccessoryView = button
        
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
    
        return renderer
    }
}
