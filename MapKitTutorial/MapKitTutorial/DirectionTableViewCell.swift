//
//  DirectionTableViewCell.swift
//  MapKitTutorial
//
//  Created by Swati Singh on 10/09/20.
//  Copyright © 2020 Thorn Technologies. All rights reserved.
//

import UIKit

class DirectionTableViewCell: UITableViewCell {

    @IBOutlet var txtLbl: UILabel!
    @IBOutlet var detailTextLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
