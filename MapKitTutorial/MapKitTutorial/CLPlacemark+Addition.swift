//
//  CLPlacemark+Addition.swift
//  MapKitTutorial
//
//  Created by Swati Singh on 11/09/20.
//  Copyright © 2020 Thorn Technologies. All rights reserved.
//

import CoreLocation

extension CLPlacemark {
  var abbreviation: String {
    if let name = self.name {
      return name
    }

    if let interestingPlace = areasOfInterest?.first {
      return interestingPlace
    }

    return [subThoroughfare, thoroughfare].compactMap { $0 }.joined(separator: " ")
  }
}
